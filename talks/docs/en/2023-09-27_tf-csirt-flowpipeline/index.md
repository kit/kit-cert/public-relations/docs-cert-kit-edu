# KIT-CERT at the 70th TF-CSIRT Meeting

## Monitoring Netflows with Flowpipeline and ELK

We had a talk covering our new approach to monitor netflows (and sflows)
with flowpipeline and the elastic stack. See the [slides](slides-crop.pdf).
