---
title: Vorstellung Modern MISP
tags: presentation, talk, vortrag
type: slide
slideOptions:
  theme: white
  progress: false
  controls: false
  slideNumber: true
paginate: true
marp: true
footer: >
  KIT-CERT @ CERT-Verbund | 2024-11-14 
  <span style="background-color:black; color: white">&nbsp;TLP:CLEAR&nbsp;</span>
---

<script type="module">
  import mermaid from 'https://cdn.jsdelivr.net/npm/mermaid@11/dist/mermaid.esm.min.mjs';
  mermaid.initialize({ startOnLoad: true });
</script>

<style>
    /* Slide Layout */
table {
    font-size: 85%;
}

 section {
        text-align: left !important;
        background-image: url("https://upload.wikimedia.org/wikipedia/commons/3/3a/Logo_KIT.svg"); 
        border: none; 
        box-shadow: none; 
        background-size: 150px; 
        background-repeat: no-repeat;    
        background-position: 98% 2.5%;
        height: 100%;
        font-size: 28px;
    }
    
    /* KIT Design Bulletpoints */
 section ul {
        margin: 0;
        padding: 0;
        list-style-type: none !important;
    }
section ul li {
    padding-left: 1.5em;
    margin: 0;
}
    
    
section ul li::before{
    z-index: -1;
    content: "   "; 
    color: #009682;
    font-size: 1.5rem;
    padding: 0;
    margin-right: 0.2em;
    line-height: 1;
    padding-right: 0.5em;
    background: url('data:image/svg+xml;utf8,<svg width="50" height="50" xmlns="http://www.w3.org/2000/svg"><path d="M10 0 H40 A10 10 0 0 1 50 10 V40 A10 10 0 0 1 40 50 H10 A10 10 0 0 1 0 40 V10 A10 10 0 0 1 10 0 Z M10 0 V10 H0 V0 H10 Z M40 50 V40 H50 V50 H40 Z" style="fill:%23009682;stroke:%23009682;stroke-width:1" /></svg>') no-repeat;
    background-size: 0.4em 0.4em;
    background-position-y: 67%;
}
h1 {
    color: black;
}

    /* More h3 bottom margin, don't overlap with slide background (KIT-Logo) */
section h3, .doc section h3 {
        margin-top: 5px;
        margin-bottom: 35px;
        max-width: calc(100% - 150px);
        font-size: 30px;
    }
    
    /* No image borders */
    .reveal section img {
        border: none;
        box-shadow: none;
    }
    
    .smaller-font > div {
        font-size: 28px;
    }
    .footnote-size {
        font-size: 12px;
    }
</style>

## Modern MISP

##### Gegenwart und Zukunft

<p class="footnote-size">(no power point were harmed in the making of this presentation)</p>

----

### Historie

- Wunsch, Threat Intelligence:
    - systematisch speichern
    - teilen
    - externe Daten einbinden
- Frühling/Sommer 23: Versuch MISP mit Postgres und NGINX
- Naive Annahme:
    - Dateien wie INSTALL/POSTGRESQL-structure.sql existieren
      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ⇒ Support für Postgres
    - CakePHP als ORM wird das schon machen 

----

#### Realität:

- `app/Model/AppModel.php`: Alle DB-Migrationen nur für MySQL abgelegt
- `.htaccess` für Rewrite-Rules
- nicht beachteter Pull Request für Postgres Support
- (teil-) gefrusteter Hiwi

----

### Vorbereitung Praxis der Softwareentwicklung (PSE)

- MISP, wie schwer kann das schon sein?
- Ziel: Trennung Frontend &harr; Backend
- Naive Annahme:
    - korrekte OpenAPI Spec
    - MISP-DB Schema vernünftig

----

### PSE

- WiSe 23/24: 1. Runde PSE:
    - Frontend (auf Basis der bestehenden MISP-API)
    - API 
    - Worker (Background Jobs)
- SoSe 24: 2. Runde PSE
    - Workflows
    - Login per OIDC & PW, User Settings, Galaxies, Server, Freetext-Import, Statistics, ...
- WiSe 24/25: 3. Runde PSE **jetzt**
    - Mehr Worker
    - Rechtemanagement in der API

----

### Architektur

<!--
API stateless
Management Worker HTTP Schnittstelle
Worker können direkt über DB schreiben


-->

<pre class="mermaid">
architecture-beta
    service db(database)[SQL DB]
    service api(internet)[API]
    service workerapi(internet)[Worker API]
    service worker(server)[Worker]
    service redis(database)[Redis]
    service frontend(cloud)[Frontend]

    api:L -- R:db
    api:B -- T:workerapi
    workerapi:L -- R:redis
    worker:B -- T:redis
    worker:R -- L:db
    frontend:L -- R:api
</pre>


----

#### Aktueller Stand (Frontend)

- Lauffähig mit MISP und Modern MISP API
- Compatibility Mode, dadurch kein:
    - Passwort Login
    - OIDC Login
    - Kleinigkeiten, wie Username usw.
- CRUD von Events, Attribute, Galaxies, Tags, User, API Keys

----

#### Aktueller Stand (Lib &mdash; DB)

- DB:
- Von Modern Misp hinzugefügt:
  - Tabellen 2
  - Indizes ~~91~~ 108
  - Foreign Key Constraints ~~27~~ 31
  - Andere Constraints ~~1~~ 3
- Noch nicht implementiert:
  - Tabellen ~~65~~ 55 
  - Indizes ~~24~~ 22
  - Spalten 2

----

#### Aktueller Stand (API)

- API Implementierung für:
    - `/attributes`
    - `/events`
    - `/galaxies` 
    - `/galaxy_clusters`
    - `/tags`
    - `/admin/`
    - &hellip;

----

#### Aktueller Stand (API) (2)

- Kompatibilitätstest für API
    - liefern beide APIs mit gleicher Datenbank das gleiche Ergebnisse?
    - bisher wenige Endpunkte getestet
- 214 Endpunkte implementiert


----

#### Aktueller Stand (Worker)

- Freetext-Import
- E-Mail Versand
- Enrichment
- Korrelation (und Overcorrelation)

<!--
Enrichment + Korrelation Plugins
-->

----

##### Performance (API)
- Eingebautes Profiling
    - ⇒ Cache für Verify Secret eingebaut
- [Cypress Test](https://cloud.cypress.io/projects/k7wfye/runs) für Frontend auf Legacy API und MMISP API
    - Legacy MISP: 02:52
    - Modern MISP: 03:16
- Unterschiedliche Anzahl an Tests
⇒ ohne große Optimierungen ungefähr gleich schnell

----

##### Skalierung

- durch das Softwaredesign ist Skalierung problemlos
- API ist stateless
    - über Loadbalancer beliebig viele Instanzen anbinden
- DB Loadbalancing ist möglich
- Frontend läuft komplett im Browser &rarr; ausliefernder Server: Raspberry PI?
- Worker: Implementiert mit Celery/Redis, kann auch beliebig skaliert werden.

----

#### Kurzfristig

- erste Synchronisationstests
- MMISP als Alternative mit reduziertem Funktionsumfang


----

<!-- _footer: '<span style="background-color:black; color: #FF2B2B">&nbsp;TLP:RED&nbsp;</span>' -->

### Ist MISP wirklich so schlimm?

Ja.

----

<!-- _footer: '<span style="background-color:black; color: #FF2B2B">&nbsp;TLP:RED&nbsp;</span>' -->

### Ist MISP wirklich so schlimm?

* Verletzung des HTTP Standards, kein Bearer in `Authorization` Header
* Settings in DB und/oder Konfigurationsdatei
    * MISP überschreibt Konfigdatei
* DB Migrationen in AppModel.php
* Riesige Dateien (`Model/Event.php` 8215, `EventsController.php` 6526)
* Generierung von Python Code, der dann ausgeführt wird
* Speicherort für Logs gefühlt random
* Background Jobs
* Rückgabe Schema in API immer leicht anders
* Features sind **irgendwo** implementiert

----

# Live Demo

<!--
localhost:4000/docs
localhost:5000/docs
localhost:5173/
-->

----

# Diskussion

## MMISP als Alternative mit reduziertem Funktionsumfang
- Welche Features werden von euch genutzt?

