.PHONY: main main_clean main_upload ssh ssh_clean ssh_upload talks talks_clean talks_upload  pse pse_upload pse_clean

all: main ssh talks pse
upload_all: main_upload ssh_upload talks_upload pse_upload
clean_all: main_clean ssh_clean talks_clean pse_clean


ssh:
	@echo ${PATH}
	$(MAKE) -C ssh
ssh_clean:
	$(MAKE) -C ssh clean
ssh_upload:
	$(MAKE) -C ssh upload

pse:
	@echo ${PATH}
	$(MAKE) -C pse
pse_clean:
	$(MAKE) -C pse clean
pse_upload:
	$(MAKE) -C pse upload

talks:
	@echo ${PATH}
	$(MAKE) -C talks
talks_clean:
	$(MAKE) -C talks clean
talks_upload:
	$(MAKE) -C talks upload

main:
	@echo ${PATH}
	$(MAKE) -C main
main_clean:
	$(MAKE) -C main clean
main_upload:
	$(MAKE) -C main upload

index_upload:
	rsync -vaP --delete-after index.php scc-web-0018@docs.cert.kit.edu:docs.cert.kit.edu/htdocs/

