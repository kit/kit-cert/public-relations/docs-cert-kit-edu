<?php
// Function to check if a folder exists
function folderExists($folderPath) {
    return is_dir($_SERVER['DOCUMENT_ROOT'] . $folderPath);
}

// Get the user's preferred languages with q-factors from the browser
$preferredLanguagesWithQ = $_SERVER['HTTP_ACCEPT_LANGUAGE'];

// Parse the languages and q-factors into an array
$languageQPairs = explode(',', $preferredLanguagesWithQ);

// Get the current path
$currentPath = dirname($_SERVER['PHP_SELF']);
if ($currentPath === "/") {
    $currentPath = "/main";
}

// Default fallback URL
$redirectURL = "{$currentPath}/de"; // Default to German
$maxQFactor = 0; // Initialize the maximum q-factor to 0

$languageCodes = [
    "de",
    "en"
];

// Iterate through language-q pairs and choose the best match
foreach ($languageQPairs as $languageQPair) {
    // Split language and q-factor
    $pairComponents = explode(';', $languageQPair);
    $language = trim($pairComponents[0]);

    // If there's a q-factor component, process it
    if (count($pairComponents) > 1) {
        $qFactorComponent = trim($pairComponents[1]);

        // Extract the q-factor value
        if (strpos($qFactorComponent, 'q=') === 0) {
            $qFactorValue = (float) substr($qFactorComponent, 2);
        } else {
            // Invalid q-factor format, default to 1
            $qFactorValue = 1;
        }
    } else {
        // No q-factor provided, default to 1
        $qFactorValue = 1;
    }

    foreach ($languageCodes as $languageCode) {
        if (stripos($language, $languageCode) === 0 && $qFactorValue > $maxQFactor) {
            $potentialURL = "{$currentPath}/{$languageCode}"; // German
            if (folderExists($potentialURL)) {
                $redirectURL = $potentialURL;
                $maxQFactor = $qFactorValue;
            }
        }
    }
}

if (!folderExists($redirectURL)) {
    echo "Sorry, cannot redirect you to a working page as the determined target {$redirectURL} does not exist.<br />";
    exit;
}

// Redirect the user
header("Location: $redirectURL");
exit;
