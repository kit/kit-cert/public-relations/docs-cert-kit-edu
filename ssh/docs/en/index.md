# SSH in a nutshell

Secure Shell (SSH) is a protocol suite to securely access services over an
unsecured network. Its most common use cases are remote login to a shell and file transfers. 

This document was written with a focus on accessing the central services
provided by SCC, such as GridKa, HPC, the central web server cluster, or the
KIT GitLab instance. For the most part, the content should be applicable to SSH
in general.

[OpenSSH](https://www.openssh.com) is the most relevant implementation today.
This document may recommend settings, which are incompatible with alternative
implementations like [Dropbear](https://matt.ucc.asn.au/dropbear/dropbear.html)
or [TinySSH](https://tinyssh.org).

The OpenSSH recommendations consist of three parts. [Client Basics](OpenSSH/basics.md)
is targeted to end users with little or no prior exposure to SSH. It is
intended to get you started with a secure configuration that is still
convenient to use. It may well be worth a read for more advanced users if you
want to learn about the following

!!! Warning "Five Rules of OpenSSH Security"
    1. Keep your computer up-to-date
    1. Be careful with `~/.ssh/*`
    1. Protect your SSH private keys
    1. Protect your `ssh-agent`
    1. Avoid using `scp` 

The remaining parts discuss more [advanced topics](OpenSSH/advanced.md) and [server
configuration](OpenSSH/server.md).

!!! Note "A Note on Cryptography" 
    Although SSH builds on cryptography, we will not discuss or recommend
    any specific settings in this document at this time. Keeping systems 
    up-to-date on both the server and client side will lead to safe 
    cryptographic defaults. If you absolutely want to tweak these settings,
    there are [projects](https://cipherlist.eu) documenting the strongest 
    possible configuration. This will lead to interoperability issues and create
    operational burden, since these settings need to be adjusted over time as
    research in cryptopgraphy yields new results.

We appreciate feedback through [GitLab issues or merge
requests](https://git.scc.kit.edu/KIT-CERT/mkdocs/ssh-en/).

The canonical URL of this document is
[https://www.cert.kit.edu/p/guide/ssh/en](https://www.cert.kit.edu/p/guide/ssh/en).

[![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)<br />
This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
