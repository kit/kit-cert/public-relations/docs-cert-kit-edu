# Other SSH clients

This document focuses exclusively on [OpenSSH](https://openssh.com) to keep it
concise and easy to maintain. OpenSSH is also the only client that is free and
available on all major (desktop) operating systems.

Due to popular demand we've included our opinions on a few alternative SSH
clients.

## MobaXterm

[MobaXterm](https://mobaxterm.mobatek.net) is a terminal application,
which serves as a SSH client and X Server for Windows operating systems.

The Home Edition can be used professionally as long as the person using
the software is the same person who downloaded and installed it.

To use the software on centrally managed or shared systems a paid license is
required.

To get started, follow the
[documentation](https://mobaxterm.mobatek.net/documentation.html).

## PuTTY

[PuTTY](https://www.chiark.greenend.org.uk/~sgtatham/putty/) is a free
implementation of SSH and Telnet for Windows and Unix platforms. It was
a widely used SSH client for Windows until the integration of OpenSSH in
Windows 10. We recommend to migrate to [OpenSSH](../OpenSSH/basics.md) if
possible. 

While the project is still actively maintained, it is somewhat cumbersome to
use and lacks support for the `ProxyJump` mechanism. It uses a proprietary
format (`.ppk`) for SSH keys.

Anectodal evidence suggests that PuTTY ist almost never updated after the
initial installation.

## Non-native OpenSSH environments for Windows

There are a few software packages that provide a UNIX-like environment for
Windows; most notably [Cygwin](https://cygwin.com/),
[MSYS2](https://www.msys2.org/) and the [Windows Subsystem for
Linux](https://docs.microsoft.com/en-us/windows/wsl/install-win10).

They lack access to the native ssh-agent and they need to be maintained
separatly from Windows.
