# References

* [OpenSSH man pages and relevant specs](https://www.openssh.com/manual.html)
* [scp deprecation notice (paragraph 4-6)](https://www.openssh.com/txt/release-8.0)
  (see also [this SO thread](https://unix.stackexchange.com/questions/571293/is-scp-unsafe-should-it-be-replaced-with-sftp)
  and [this LWN post](https://lwn.net/SubscriberLink/835962/ae41b27bc20699ad/))

!!! note
    The first link points to the documentation for the current version of
    OpenSSH. Your will most likely interact with older versions that provides a
    different set of options. When in doubt please consult your local manpages
    (`man ssh-xxx`). Be advised that your client's version will likely differ
    from the server's version.

# Further readings

## SSH

* [Mozilla SSH guidelines](https://infosec.mozilla.org/guidelines/openssh)
* [SSH Agent Explained](https://smallstep.com/blog/ssh-agent-explained/)
* [Fedora Magazine: SCP user’s migration guide to rsync](https://fedoramagazine.org/scp-users-migration-guide-to-rsync/)

## Cryptography

* [Cipherlist.eu](https://cipherlist.eu/)
* [Applied Crypto Hardening](https://bettercrypto.org/)

