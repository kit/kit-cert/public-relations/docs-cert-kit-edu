# Advanced Topics

## Using the `~/.ssh/config` file

The OpenSSH client behavior can be controlled with both command line options
and configuration settings in `~/.ssh/config`. Setting up a config file can
save some typing.

Consider your local username being `grace` but you are known as `hopper` on the host `common.business-oriented.language.kit.edu` and you are also required to use a special private key `~/.ssh/cobol.id_ed25519` to authenticate.

The command line call will look like this:

```nohighlight
ssh -i .ssh/cobol.id_ed25519 hopper@common.business-oriented.language.kit.edu
```

To abbreviate this to `ssh cobol` add the following to `~/.ssh/config`:

```nohighlight
Host cobol
  HostName     common.business-oriented.language.kit.edu
  User         hopper
  IdentityFile ~/.ssh/cobol.id_ed25519
```

For other options consult the documentation in the `ssh_config` man page.  Your
service provider may provide some useful snippets as well. Again, be careful
with the settings you make, only copy them from trusted sources or when you
fully understand their implications.


## Bastion Hosts

Some setups have the concept of a bastion or jump host, which acts as the
single point of entry to the systems beyond. In the past, this was one use
case for SSH agent forwarding, which creates tremendous security risks, should
the bastion host ever be compromised.

OpenSSH 7.3+ has a feature called `ProxyJump` as a safe alternative to agent
forwarding. If you want to connect to `backend.internal.kit.edu` through a jump
host called `jump.internal.kit.edu`, use something like:

```nohighlight
$ ssh -J jump.internal.kit.edu backend.internal.kit.edu
```

This can also be added to `~/.ssh/config`:

```nohighlight
Host *.internal.kit.edu !jump.internal.kit.edu
  ProxyJump jump.internal.kit.edu
```


## Automated SSH Logins

For automation purposes, unattended SSH logins may be necessary. This is one of
the few exceptions where private keys without a passphrase are acceptable.

As a security measure, create new keys for each automation job and limit their
privileges on the server. See the `sshd` man page for details about the
`authorized_keys` file format.

```nohighlight
$ cat ~/.ssh/authorized_keys
restrict,command="uptime" ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAID9icauGFYzZCWe7P6tVH9/70pGON39H+CVB0DEY2PSg uptime_key
```

```nohighlight
$ ssh -T user@host.kit.edu
 11:07:28 up 24 days, 15:29,  0 users,  load average: 0.02, 0.05, 0.02
Connection to host.kit.edu closed.
```

`ssh -T` does not try to allocate a pseudo-terminal, which would fail anyway
because of the `restrict` key option on the server.  Without it, the connection
will still succeed but the following warning will always be printed:

```nohighlight
PTY allocation request failed on channel 0
```

!!! Attention
    Older versions of OpenSSH do not recognize the `restrict` option. It that
    is the case substitute it with
    `no-agent-forwarding,no-port-forwarding,no-pty,no-user-rc,no-X11-forwarding`
