# OpenSSH Server

Some of the following examples are simplified to focus on the core concepts or
to illustrate certains aspects. Please make sure that you fully understand what
you are doing instead of blindly copying snippets from this page. When in
doubt, please consult the [official
documentation](https://www.openssh.com/manual.html).

## Using SSH certificates

The trust-on-first-use security model does not provide a good user experience
and can encourage bad habits, when users delete their `~/.ssh/known_hosts` file
to get rid of warnings about changed keys.  

OpenSSH also supports a trust model similar to X.509 certificates. A SSH key
pair can be used to sign host keys as a certificate authority (CA). Only this
key needs to be trusted by clients to establish trust to all host keys signed.

Take extra precautions to keep the CA's private key secure. The usual
approaches are to use a hardware security module (HSM) or keep the keys on an
offline computer.

The following example creates a CA and signs a host key. See the `ssh-keygen`
man page for details on the command line options.

Create a CA key pair:

```sh
mkdir -m 0700 ~/ssh-ca
ssh-keygen -q -t ed25519 -f ~/ssh-ca/server_ca -C server_ca
```

Create a keypair for a host and sign it:

```sh
mkdir -m 0700 ~/ssh-host-keys
ssh-keygen -q -N '' -t ed25519 -f ~/ssh-host-keys/host1_ed25519 -C host1
ssh-keygen -s ~/ssh-ca/server_ca -I host1 -h -n host1.cluster.kit.edu -V +52w ~/ssh-host-keys/host1_ed25519.pub
```

Upload the private key and the certificate to the server's `/etc/ssh/` directory and add the following
to `sshd_config`:

```nohighlight
HostCertificate /etc/ssh/host1_ed25519-cert.pub
```

Now instruct you users to add the CA certificate to their known hosts file
(either `~/.ssh/known_hosts` or `/etc/ssh/ssh_known_hosts`):

```nohighlight
@cert-authority *.cluster.kit.edu ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAID9icauGFYzZCWe7P6tVH9/70pGON39H+CVB0DEY2PSg server_ca
```

Despite the absence of hosts1's public key, the client will connect to
`host1.cluster.kit.edu` without a warning.

The certificate authority concept can also be applied to sign and revoke user's
public keys.

The signature in this example is valid for a limited time (52 weeks, `-V +52w`)
to ensure that host keys are renewed regularly.

!!! Important "This is only a simplified description of SSH certificate authorities"
    Please consult the [documentation](https://www.openssh.com/manual.html)
    before rolling out an SSH certificate authority! To keep this document
    short we skipped details like HSMs, key revocation lists, serial numbers,
    and restriction mechanisms like principals and options.

## SFTP-only access

OpenSSH can be restricted to allow certain users exchanging files with SFTP
without the ability to run shell commands.

It is advisable to create separate user accounts with a login shell of
`/bin/false` and a special group for this use case.

```sh
GROUPNAME=sftpaccess
USERNAME=filetransfer
groupadd --system "${GROUPNAME}"
useradd -c "sftp only group" -G "${GROUPNAME}" --create-home --system --shell /bin/false "${USERNAME}"
```

Now add the following snippet to `sshd_config` and customize it to fit your
environment:

```nohighlight
Subsystem sftp internal-sftp

Match Group sftpaccess
    AllowAgentForwarding no
    AllowTcpForwarding no
    PermitTunnel no
    X11Forwarding no
    ForceCommand internal-sftp
```

To log all user actions, change the line into `ForceCommand
internal-sftp -l VERBOSE`.

You can use `ChrootDirectory` to further isolate these accounts. 

!!! Attention 
    Setting `ChrootDirectory` to `%h` allows users to add or remove ssh-keys to their
    account unless `~/.ssh/authorized_keys` is proteced by changing ownership and
    removing write-permissions. Alternativly set `AuthorizedKeysFile` to a place
    outside the user's reach.

`ChrootDirectory` requires certain restrictions regarding ownership and permissions
of the chroot directory. Please see the `sshd` man page for further information.
