# OpenSSH Client Basics

## Prerequisites

The goal is to provide a secure configuration without compromising convenience.
Our approach is opinionated and advanced users may object to some of the
recommendations.

This document assumes at least [OpenSSH](https://www.openssh.com/) version 7.3
on the client side, which was released in August of 2016. Older versions do not
support some of the features required for the recommendations below. The
minimal required server version is OpenSSH 6.5, released in January of 2014.

The OpenSSH client is included in all major operating systems. The following
lists the minimal release for popular operating systems which should work
out-of-the-box with this guide: 

* Windows 10 / Server 2019 1809 aka "April 2018 Update"
* macOS 10.12+
* RedHat/CentOS/Scientific Linux 7+
* Ubuntu 18.04+
* Debian 9+
* SLES 15+.

To check the installed OpenSSH version, open a terminal window and run `ssh -V`.

If your system ships with an older version, you should consider an operating
system upgrade or talk to the person responsible for managing your computer.

Running a current version of the SSH client serves two purposes: It will
protect you from known vulnerabilities and it will ensure no outdated
cryptography will be used.

!!! Important "Rule 1: Keep your computer up-to-date"
    [Automatically installing the provided operating system patches by the
    vendor](https://www.scc.kit.edu/sl/patch) is the single most important
    aspect of maintaining computer security.


## Getting started

The first step in a SSH connection is the server proving its identity to the
client. SSH normally uses a trust-on-first-use security model. This means,
when you connect to a remote host for the very first time on any computer, the
client lets you decide to trust the remote server by showing a prompt like
below:

```nohighlight
$ ssh example.kit.edu
The authenticity of host 'example.kit.edu (2001:db8::1)' can't be established.
ECDSA key fingerprint is SHA256:9ukznkQICXJ+khNIKXeAgkAfHm87CHrEyqCpHrHQYjg.
Are you sure you want to continue connecting (yes/no/[fingerprint])?
```

If the remote server's fingerprint is documented publicly, e.g. on a website,
you can manually verify the correctness. If you accept the fingerprint with
'yes', the client will store the fingerprint locally in `~/.ssh/known_hosts`
and subsequent connections will only succeed if the fingerprint does not
change.

Should you ever see the warning below on a centrally managed system, please
contact the administrator of the remote system. The system's host key may have
been changed on purpose or by accident. It is also possible that an attacker is
trying to hijack your connection. 

```nohighlight
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the ECDSA key sent by the remote host is
SHA256:9ukZNKQICXJ+kwNikxeAgkAfHm07CHrEyqCpHrHQYjg.
Please contact your system administrator.
Add correct host key in /home/example/.ssh/known_hosts to get rid of this message.
Offending ECDSA key in /home/example/.ssh/known_hosts:14
ECDSA host key for example.kit.edu has changed and you have requested strict checking.
Host key verification failed.
```

You should now be able to login to the remote server with your username and password. 

```nohighlight
$ ssh ab1234@example.kit.edu
ab1234@example.kit.edu's password:
```

Some service providers may prohibit login with username and password for
security reasons. In that case, follow the steps below to create a key pair and
follow the provider's documentation to upload the public key to their system.

It is possible that your service provider uses a certificate authority to
improve the trust-on-first-use model. In that case they will document a
snippet to add to your local `~/.ssh/known_hosts` file to avoid the unknown
host warnings. 

!!! Important "Rule 2: Be careful with ~/.ssh"
    Only change the content of `~/.ssh` if you understand all the implications or
    when you are instructed from a trusted source.


## SSH key pairs

SSH key pairs are a more secure alternative to logins with username and
password. We will see later, how they can save a lot of typing in combination
with a key manager like `ssh-agent`.

To create your first key pair, run

```nohighlight
ssh-keygen -t ed25519
```

Accept the default file name and pick a secure passphrase. If in doubt, follow
the KIT password policy ([password generator](https://www.cert.kit.edu/p/PasswordGenerator)).
Do not re-use a password from another system.

Under special circumstances unencrypted keys can be
[useful](advanced.md#automated-ssh-logins) for automation or machine-to-machine
communication. Never use them for interactive logins and day-to-day operation.
Using keys without a passphrase may be administratively prohibited for specific
environments (e.g. HPC, GridKa).

A key pair consists of a private or secret key, which remains on your system,
and the public key, which is installed on the remote system. Only share your
private key between your own systems, like your desktop and laptop. There
should be no reason to upload them to systems like web servers or HPC login
nodes. It is completely fine to have multiple SSH key pairs for different
purposes.

If you are able to login to the remote system with username and password,
upload your public key like this:

```nohighlight
ssh-copy-id user@server
```

This will add your public key to the file `~/.ssh/authorized_keys` on the
remote server. If your service provider does not allow logins with username and
password or decided to centrally manage the content of the `authorized_keys`
file, the service provider will provide documentation on how to manage your keys.

Next time you login to the remote server, you should be asked for your private
key passphrase instead of your password.


!!! Important "Rule 3: Protect your SSH private keys"
    Always encrypt your private keys with a strong passphrase. Keep your private
    keys secure. Ideally, they should never leave your personal system.


## Using `ssh-agent` 
 
SSH key managers allow to use private keys with strong passphrases and avoid
typing the passphrase on every connection.

On Linux and macOS systems `ssh-agent` is usually started when you log in to the desktop environment.
session.

Windows systems provide a service for the same purpose. It is not automatically
started and requires a configuration change. These require administrative
privileges. If you do not have these on your system, ask the person responsible
for your client to configure the service for automatic start.

The official Microsoft documentation explains the necessary
[steps](https://docs.microsoft.com/en-us/windows-server/administration/openssh/openssh_keymanagement). Run the following command in an elevated PowerShell:

```nohighlight
Get-Service -Name ssh-agent | Set-Service -StartupType Automatic
```

You can add your private key to your agent by calling `ssh-add`.

Type your passphrase to decrypt the private key. As long as your key is loaded
in your agent, you do not need to type the passphrase when you log in to the
remote system. When you log out of your local system, `ssh-agent` quits and all
your keys are inaccessible without the passphrase.

It is possible to forward your agent to remote systems. The feature is disabled by
default and for good reason. A privileged user on a remote system can access
your agent for a so-called impersonation attack.


!!! Important "Rule 4: Protect your `ssh-agent`"
    Never forward your agent to remote systems. 


## File transfers with SSH

The standard utility to copy files to and from remote systems is `scp`. The
protocol has some design issues and the OpenSSH maintainers [recommend to use
alternatives](https://www.openssh.com/txt/release-8.0) like `sftp` or `rsync`.

If you had no prior experience with SSH, it is advisable to start with one of
the alternatives instead of learning a tool that is already considered
deprecated.

!!! Important "Rule 5: Avoid using `scp`"
    Use `sftp` or `rsync` for file transfers over SSH.


## Summary

This concludes the basics and will provide you with a working and secure setup
for your OpenSSH client. If you want to learn more, please read on in the
[advanced topics section](advanced.md).
