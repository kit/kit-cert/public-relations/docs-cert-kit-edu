# Praxis der Softwareentwicklung

## Wintersemester 2024 / 2025
<!---
### Python Insights

* Generierung von UML (Klassen-)Diagrammen (Plantuml / Mermaid)
* Modellierung von Modulen als Objekte/Klassen?
* Anzeigen von Intra-Dependencies:
    * Packageebene
        * welches Packages greift auf welches Package zu?
    * Modulebene

---

-->

### Modern MISP -- API

In MISP können verschiedene Organisationen ihre Threat Intelligence speichern.
Bedrohungsinformationen können beispielsweise Prüfsummen von Malware oder IP-Adressen,
von denen Attacken ausgingen, sein.

Modern MISP ist eine Neuimplementierung von MISP unter der MIT Lizenz in Python.
Der Technologiestack besteht aus Fastapi, Pydantic, Sqlalchemy, Celery im Backend und SvelteKit mit TailwindCSS im Frontend.
Modern MISP steht unter der MIT Lizenz.

Im Kern dieses PSE Projekts stehen Änderungen an der API, geringfügige Änderungen 
am Frontend oder den Workern können aber, je nach Zielsetzung durch das Team, auch erfolgen.

#### Mandantenfähigkeit

Abhängig von den beteiligten Organisation, der Sharing Group, dem Status der Veröffentlichung
können Nutzer Events und Attribute in MISP entweder gar nicht, oder nur mit einer eingeschränkten sehen.
Diese Bedingungen sollen entdeckt, implementiert und Testfälle entworfen werden, 
die Kompatibilität zwischen Legacy und Modern MISP sicherstellen. 

Dadurch soll auch das rollenbasierte Rechtesystem aus legacy MISP, das prinzipiell bereits implementiert wurde,
in Modern MISP übertragen werden.

#### UUID

Zusätzlich soll die API angepasst werden, sodass Events und Attribute über ihre UUIDs 
und nicht nur über Datenbank-ID abgefragt werden können.

#### Analyst Data (optional)

Zu Analyst Data schreiben die MISP Entwickler:

> Analyst Data allows analysts to share and add their own analysis to any MISP data having an UUID. They can be one of these three type: Analyst Note, Analyst Opionion or Analyst Relationship.

Analyst Data sind noch nicht in Modern MISP implementiert.
Dafür müssen API Schnittstellen in legacy MISP gefunden werden, die es erlauben Analyst Data hinzuzufügen oder zu entfernen. Diese Schnittstellen können dann über die API benutzt werden.

---

### Modern MISP -- Worker

Dieses Projekt fokusiert die Weiterentwicklung der Worker.

#### Galaxy, Taxonomy, and Objects Import

* https://github.com/MISP/misp-galaxy
* https://github.com/MISP/misp-taxonomies
* https://github.com/MISP/misp-objects

Galaxies erlauben das strukturierte Hinzufügen von (Meta-)Informationen zu Attributen und Events.
Dafür stellt MISP Projekt Daten bereit, die momentan aber noch nicht in Modern MISP importiert werden können.
Dieses PSE Projekt soll das Worker Repository so erweitern, dass periodisch die Daten importiert werden können.
Ähnliches kann zudem für die Taxonomien und die Object Templates erfolgen.

#### Feeds

Quoting [CIRCL](https://www.circl.lu/doc/misp/managing-feeds/#feeds):
> Feeds are remote or local resources containing indicators that can be automatically imported into MISP at regular intervals. Feeds can be structured in MISP format, CSV format or even free-text format. You can easily import any remote or local URL to store the data in your MISP instance. It's a simple way to gather many external sources of information into MISP without any programming skills.

Feeds sind derzeit nicht in Modern MISP implementiert. 
Dieses PSE soll das Importieren von Feeds über einen Worker implementieren.
Ebenso soll das API-Repository angepasst werden, sodass die Schnittstellen eines legacy MISP Systems funktionieren.


#### Worker Management

In Modern MISP ist es bisher nicht über die Weboberfläche möglich, Jobs und Worker zu monitoren, oder wiederkehrende Jobs zu schedulen.
In Celery ist ein Scheduler integriert, der mit dem bestehenden legacy MISP Scheduler sychronisiert werden müsste.

---

<!---

### CERT Process Management Tool

* Tool um die Arbeitsabläufe im CERT zu standardisieren.
* In simpler Form: Checkliste
* In ausgewachsener Form: BPMN
* Definition der Prozesse über YAML.
* Laden der Prozesse aus GIT.
* Einbinden von Web-APIs soll möglich sein.
    * Zum Untersuchen von Daten
    * Mit Prompt von [JQ-Web](https://github.com/fiatjaf/jq-web)/[Cyberchef](https://gchq.github.io/CyberChef/)?
    * Zur Speicherung im Ticketsystem

#### Mögliche Prozessdefinition

```YAML
process_name: Compromised account
version: 5
type: checklist

process:
  - Lock account
  - name: Get VPN logs
    service: logcollector
```

-->
## Sommersemester 2024

### Modern MISP -- Workflows

[MISP Threat Sharing](https://www.misp-project.org/) ist die vorherschende Plattform um 
Threat Intelligence, also Informationen über Angreifer, wie beispielsweise IP-Adressen,
Prüfsummen von Dateien oder Mailadressen zu teilen.

In dem PSE WS 23/24 wurden bereits große Teile von MISP in eine Neuentwicklung
überführt, die nun noch ergänzt werden sollen. Die Neuentwicklung "Modern MISP"
ist API- und daten-kompatibel, das Frontend kann auf einer bestehenden MISP Implementierung
genutzt werden und die API kann mit einer bestehenden Datenbank arbeiten.

In diesem PSE Projekt sollen nun Workflows implementiert werden.
In den Workflows ist es möglich, bestimmte Aktionen zu definieren, die nach Triggern,
wie den Speichern von Attributen oder dem Pullen von Events eines Servers durchgeführt
werden sollen.

Eine anfängliche Implementierung für Workflows im Frontend existiert bereits.
Die kompatible API muss für Modern MISP implementiert werden. Außerdem sind Änderungen an den
Workern, den Background Tasks notwendig.

---

### Modern MISP -- Feeds, Proposals, Statistics, ...

Ziel dieses PSE Projekts ist es, die Ergebnisse der drei früheren PSE Projekte aneinander
anzupassen. Aber auch einige Funktionalitäten, die bisher überhaupt nicht implementiert wurden,
können implementiert werden.

Hierbei handelt es sich um eine Fullstack Aufgabe. 
Der Technologiestack besteht aus: SvelteKit, Python, Fastapi, Sqlalchemy, Celery und Redis.
Durch die bereits vorhandenen Implementierung ist aber auch ein Einstieg möglich, es werden keine Vorerfahrungen
in jedem Aspekt erwartet. Je nach Vorlieben kann der Schwerpunkt unterschiedlich gesetzt werden.

Eine Liste der Punkte, die bearbeitet werden können:

* Galaxy Clusters
* Users
* Servers
* Warninglists
* Noticelists
* Roles
* Organisations
* Tags
* Feeds
* Proposals
* Statistics

<!--
---

### Inventree - Ausleihen

[Inventree](https://inventree.org/) ist eine Software um ein Inventar zu verwalten.
Neben einer Webapp bietet Inventree auch eine App für Android.
Durch dieses PSE Projekt soll es möglich werden, bestimmte Items aus dem Inventar zu verleihen.

Inventree unterscheidet zwischen Items, bei denen eine bestimmte Anzahl zur Verfügung steht,
die aber untereinander nicht unterscheidbar sind, beispielsweise Glühweintassen, und unterscheidbaren
Items, beispielsweise Lautsprechern.

Ziel des PSE Projekts ist es, zunächst in der Webapp das Ausleihen zu ermöglichen, inklusive Checks,
die verhindern, dass Items gleichzeitig doppelt ausgeliehen werden. Mögliche optionale Features 
sind die Einbindung in Caldav Kalender oder die Implementierung in der Inventree Android APP.

-->
